// @flow
import { loadVoyagesData } from '../../api/virail';
import { API_REQUEST } from '../apiAction';
import type { ApiRequestT } from '../apiAction';

const LOAD: 'virail/LOAD' = 'virail/LOAD';
export const LOAD_SUCCESS: 'virail/LOAD_SUCCESS' = 'virail/LOAD_SUCCESS';
const LOAD_FAILURE: 'virail/LOAD_FAILURE' = 'virail/LOAD_FAILURE';

type LoadT = {
  type: typeof LOAD,
};
export type LoadSuccessT = {
  type: typeof LOAD_SUCCESS,
  result: Array<{}>,
};
type LoadFailureT = {
  type: typeof LOAD_FAILURE,
  error: {
    message: string,
  },
};

type ActionTypeT = LoadT | LoadSuccessT | LoadFailureT;

export type StateT = {
  loading: boolean,
  loaded: boolean,
  error: ?string,
  data: Object[],
};

const initialState: StateT = {
  loading: false,
  loaded: false,
  error: null,
  data: [],
};

export default function virailDataReducer(
  state: StateT = initialState,
  action: ActionTypeT,
): StateT {
  switch (action.type) {
    case LOAD:
      return {
        ...initialState,
        loading: true,
      };

    case LOAD_SUCCESS:
      return {
        ...initialState,
        loaded: true,
        data: action.result,
      };

    case LOAD_FAILURE:
      return {
        ...initialState,
        error: action.error.message,
      };

    default:
      return state;
  }
}

export function loadVoyages(): ApiRequestT<Array<{}>> {
  return {
    type: API_REQUEST,
    types: [LOAD, LOAD_SUCCESS, LOAD_FAILURE],

    call: loadVoyagesData,
  };
}
