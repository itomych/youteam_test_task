// @flow
import { combineReducers } from 'redux';

import virail from './virail';

export default combineReducers({
  virail,
});
