// @flow

export const API_REQUEST: 'API_REQUEST' = 'API_REQUEST';

export type ApiRequestT<M> = {
  type: typeof API_REQUEST,
  types: Array<string>,
  call: () => Promise<M>,
};
