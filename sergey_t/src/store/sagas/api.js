// @flow
import { takeEvery, put } from 'redux-saga/effects';

import { API_REQUEST } from '../apiAction';

function* api({ types, call, ...rest }) {
  const [REQUEST, SUCCESS, FAILURE] = types;

  yield put({ ...rest, type: REQUEST });

  try {
    const result = yield call();
    yield put({ ...rest, type: SUCCESS, result });
  } catch (error) {
    yield put({ ...rest, type: FAILURE, error });
  }
}

// f(): Generator<Yield, Return, Next>
export default function*(): Generator<*, *, *> {
  yield takeEvery(API_REQUEST, api);
}
