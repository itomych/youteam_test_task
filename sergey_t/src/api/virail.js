// @flow
function formatDate(date: Date): string {
  return date.toISOString().split('T')[0];
}

function composeUrl(date: string) {
  // eslint-disable-next-line max-len
  const url = `https://www.virail.com/virail/v7/search/en_us?from=c.3173435&to=c.3169070&lang=en_us&dt=${date}&currency=USD&adult_passengers=1`;
  return `https://api.codetabs.com/v1/proxy?quest=${encodeURIComponent(url)}`;
}

// eslint-disable-next-line import/prefer-default-export
export function loadVoyagesData(data = new Date()) {
  const d = new Date(data);

  const batch = Array.from({ length: 7 }).map(() => {
    const dateStr = formatDate(d);

    const request = fetch(composeUrl(dateStr))
      .then(r => r.json())
      .then(schedule => {
        const voyages = schedule.result
          .filter(a => !!a.priceVal)
          .sort((a, b) => a.priceVal - b.priceVal);
        return {
          ...voyages[0],
          date: dateStr,
        };
      });

    d.setDate(d.getDate() + 1);

    return request;
  });

  return Promise.all(batch);
}
