// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';

import { loadVoyages } from '../../store/modules/virail';

function getDeparture(voyage) {
  const segment = voyage.segments[0];

  return {
    departure: segment.departure,
    fromTime: segment.fromTime,
  };
}

function getArrival(voyage) {
  const segment = voyage.segments[voyage.segments.length - 1];

  return {
    arrival: segment.arrival,
    toTime: segment.toTime,
  };
}

type PropsT = {
  loaded: boolean,
  voyages: Object[],
  error: ?string,

  loadVoyages: typeof loadVoyages,
};

class App extends Component<PropsT> {
  componentDidMount() {
    this.props.loadVoyages();
  }

  renderData() {
    const { voyages } = this.props;

    if (voyages.length === 0) {
      return (
        <TableRow>
          <TableCell colSpan="6" align="center">
            There is no data to show.
          </TableCell>
        </TableRow>
      );
    }

    return voyages.map(voyage => {
      const departure = getDeparture(voyage);
      const arrival = getArrival(voyage);

      return (
        <TableRow key={voyage.date}>
          <TableCell>{voyage.date}</TableCell>
          <TableCell>{voyage.transport}</TableCell>
          <TableCell>
            {departure.departure}
            <br />({departure.fromTime})
          </TableCell>
          <TableCell>
            {arrival.arrival}
            <br />({arrival.toTime})
          </TableCell>
          <TableCell>{voyage.duration}</TableCell>
          <TableCell>{voyage.price}</TableCell>
        </TableRow>
      );
    });
  }

  renderProgress() {
    if (this.props.error) {
      return (
        <TableRow>
          <TableCell colSpan="6" align="center" color="error.main">
            <Box color="error.main">Sorry, an error occurred. Try again later.</Box>
          </TableCell>
        </TableRow>
      );
    }

    return (
      <TableRow>
        <TableCell colSpan="6" align="center">
          <CircularProgress />
        </TableCell>
      </TableRow>
    );
  }

  render() {
    const { loaded } = this.props;

    return (
      <Box>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Transport</TableCell>
                <TableCell>Departure</TableCell>
                <TableCell>Arrival</TableCell>
                <TableCell>Duration</TableCell>
                <TableCell>Price</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{loaded ? this.renderData() : this.renderProgress()}</TableBody>
          </Table>
        </TableContainer>
      </Box>
    );
  }
}

export default connect(
  ({ virail }) => ({ loaded: virail.loaded, voyages: virail.data, error: virail.error }),
  {
    loadVoyages,
  },
)(App);
