import React from "react";

import ConnectionsList from "../components/ConnectionsList";

export default function ConnectionsPage() {
  return <ConnectionsList />;
}
