import React from "react";
import { Jumbotron, Button } from "react-bootstrap";

export default function HomePage() {
  return (
    <>
      <Jumbotron>
        <h1>Hello, there!</h1>
        <p>
          Here you can find the detailed info (departure place and time,arrival
          place and time, price, duration, etc) of the cheapest connection per
          day, for the next 7 days
        </p>
        <p>
          <Button variant="primary">Check</Button>
        </p>
      </Jumbotron>
    </>
  );
}
