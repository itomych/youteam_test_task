import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Layout from "./components/Layout";
import HomePage from "./containers/HomePage";
import ConnectionsPage from "./containers/ConnectionsPage";

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/connections" component={ConnectionsPage} />
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
