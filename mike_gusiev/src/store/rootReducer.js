import { combineReducers } from "redux";
import connectionsReducer from "./connections/reducer";

export default combineReducers({
  connectionsReducer
});
