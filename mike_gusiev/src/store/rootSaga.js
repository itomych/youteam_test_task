import { takeEvery } from "redux-saga/effects";

import { getConnectionsSaga } from "./connections/saga";
import * as types from "./connections/types";

export function* watchConnections() {
  yield takeEvery(types.FETCH_CONNECTIONS_REQUEST, getConnectionsSaga);
}
