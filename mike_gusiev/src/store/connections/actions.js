import * as types from "./types";

export const fetchConnectionsRequest = () => {
  return {
    type: types.FETCH_CONNECTIONS_REQUEST
  };
};

export const fetchConnectionsSuccess = connections => {
  return {
    type: types.FETCH_CONNECTIONS_SUCCESS,
    payload: connections
  };
};

export const fetchConnectionsFailure = error => {
  return {
    type: types.FETCH_CONNECTIONS_FAILURE,
    payload: error
  };
};
