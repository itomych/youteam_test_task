import axios from "../../axiosInstance";
import { put, call } from "redux-saga/effects";

import { fetchConnectionsSuccess, fetchConnectionsFailure } from "./actions";
import { getDates } from "../../utils";

export function* getConnectionsSaga() {
  let connections = [];
  const dates = getDates();

  for (const date of dates) {
    try {
      const response = yield call(
        axios.get,
        `virail/v7/search/en_us?from=c.3173435&to=c.3169070&lang=en_us&dt=${date}&currency=USD&adult_passengers=1`
      );

      if (response.status === 200) {
        const resultsWithPriсe = response.data.result.filter(item => {
          return item.price;
        });

        const sortedResults = resultsWithPriсe.sort((a, b) => {
          if (a.price && b.price) {
            return parseFloat(a.price) - parseFloat(b.price);
          }
          return 1;
        });

        const cheapestConnection = sortedResults[0];

        connections.push({
          date: date,
          info: cheapestConnection
        });
      }
    } catch (err) {
      yield put(fetchConnectionsFailure("error"));
    }
  }

  yield put(fetchConnectionsSuccess(connections));
}
