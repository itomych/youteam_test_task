import * as types from "./types";

const initialState = {
  connections: [],
  loading: false,
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_CONNECTIONS_REQUEST:
      return {
        ...state,
        loading: true,
        error: null
      };
    case types.FETCH_CONNECTIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        connections: action.payload
      };
    case types.FETCH_CONNECTIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    default:
      return state;
  }
};
