export function getDates() {
  const dates = [];
  const startDate = new Date();

  for (let i = 0; i < 7; i++) {
    const currentDate = new Date();
    currentDate.setDate(startDate.getDate() + i);
    dates.push(
      currentDate.getFullYear() +
        "-" +
        (currentDate.getMonth() + 1) +
        "-" +
        currentDate.getDate()
    );
  }

  return dates;
}
