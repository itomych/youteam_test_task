import axios from "axios";

const instance = axios.create({
    baseURL: 'https://www.virail.com/'
});

export default instance;
