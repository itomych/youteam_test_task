import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Table } from "react-bootstrap";

import Spinner from "./Spinner";

import { fetchConnectionsRequest } from "../store/connections/actions";

function ConnectionsList(props) {
  const { connections, loading, error, fetchConnections } = props;

  useEffect(() => {
    fetchConnections();
  }, []);

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return <p>Something went wrong :(</p>;
  }

  return (
    <div>
      {connections && connections.length && (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Date</th>
              <th>Transport</th>
              <th>Departure</th>
              <th>Arrival</th>
              <th>Duration</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {connections.map(connection => {
              debugger;
              return (
                <tr key={connection.date}>
                  <td>{connection.date}</td>
                  <td>{connection.info.transport}</td>
                  <td>{connection.info.segments[0].departure}</td>
                  <td>{connection.info.segments[0].arrival}</td>
                  <td>{connection.info.duration}</td>
                  <td>{connection.info.price}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      )}
    </div>
  );
}

ConnectionsList.propTypes = {
  connections: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.string,
  fetchConnections: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    connections: state.connectionsReducer.connections,
    loading: state.connectionsReducer.loading,
    error: state.connectionsReducer.error
  };
};

const mapDispatchToProps = dispatch => ({
  fetchConnections: () => dispatch(fetchConnectionsRequest())
});

export default connect(mapStateToProps, mapDispatchToProps)(ConnectionsList);
